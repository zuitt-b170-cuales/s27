const http = require("http");

const server = http.createServer((request, response) => {
	if(request.url === "/") {
		response.writeHead(200, {"Content-Type": "text/plain"})
		response.end("Welcome to the login page.")
	} 
	if(request.url === "/profile") {
		response.writeHead(200, {"Content-Type": "text/plain"})
		response.end("Welcome to your profile!")
	}
	if(request.url === "/courses") {
		response.writeHead(200, {"Content-Type": "text/plain"})
		response.end("Here's our courses available:")
	}
	if(request.url === "/addcourse") {
		response.writeHead(200, {"Content-Type": "text/plain"})
		response.end("Add a course to our resources")
	}
	if(request.url === "/updatecourse") {
		response.writeHead(200, {"Content-Type": "text/plain"})
		response.end("Update a course to our resources")
	}
	if(request.url === "/archivecourses") {
		response.writeHead(200, {"Content-Type": "text/plain"})
		response.end("Archive courses to our resources")
	}
}).listen(4000);

console.log("Server is running at 4000");