const http = require("http");

const port = 4000;

const server = http.createServer((request, response) => {
	// http method of the incoming requests can be accessed via "req.method"
		// GET method - retrieving / reading information; default method

	/*
		POST, PUT, and DELETE operation do not work in the browser unlike the GET method. With this, Postman solves the problem by simulating a frontend for the developers to test their codes.
	*/
	if (request.url === "/items" && request.method === "GET"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Data retrieved from the database");
	}

	if (request.url === "/items" && request.method === "POST"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Data to be sent to the database");
	}

	if (request.url === "/items" && request.method === "PUT"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Update resources");
	}

	if (request.url === "/items" && request.method === "DELETE"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Delete resources");
	}
})


server.listen(port);

console.log(`Server is running at localhost: ${port}`);