const http = require("http");

// Mock Database

let database = [
	{
		"name": "Brandon",
		"email": "brandon@mail.com"
	},
	{
		"name": "Jobert",
		"email": "jobert@mail.com"
	}
]

http.createServer((request, response) => {
	if(request.url === "/users" && request.method === "GET"){
		response.writeHead(200, {"Content-Type": "_application/json"});
		// response.write() function is used to print what is inside the parameters as a response
		// Server to Client - stringify
		// Client to Server - parse
		// Input has to be in a form of string that is why JSON.stringify is used. The data that will be received by the users/client from the server will be in a form of stringified JSON
		response.write(JSON.stringify(database));
		response.end();
	}

	if(request.url === "/users" && request.method === "POST"){
	let requestBody = ""
	/*
		data stream - flow/sequence of data

			data step - data is received from the client and is processed in the stream called "data" where the code/statements will be triggered.

			end step - only runs after the request has completely been sent once the data has already been processed
	*/
	request.on("data", function(data){
		// data will be assigned as the value of the requestBody
		requestBody += data
	});
	request.on("end", function() {
						// data here is still a string
		// the server needs an object to arrange information in the database more efficiently that is why we need JSON.parse
		requestBody = JSON.parse(requestBody)
						// data here is now an object

		let newUser = {
			"name": requestBody.name,
			"email": requestBody.email
		}
		database.push(newUser);
		console.log(database);

		response.writeHead(200, {"Content-Type": "_application/json"});
		// the client needs string data type for easier readability, that is why we should use JSON.stringify
		response.write(JSON.stringify(newUser));
		response.end();
	})
}
}).listen(4000)




console.log("Server is running at 4000");

/*
	save function instead of push
*/